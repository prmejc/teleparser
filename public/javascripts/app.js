var telegramParserApp = angular.module('telegramParserApp', [
    'ngRoute',
    'angularFileUpload',
    'telegramParserAppControllers',
    'telegramParserAppServices'
]);

telegramParserApp.config(['$routeProvider',
    function($routeProvider) {

        $routeProvider = indexRP($routeProvider);

        function indexRP(rp) {
            rp.
            when('/test', {
            templateUrl: 'assets/partials/test.html',
            controller:  'IndexCtrl'
            }).
            when('/', {
                templateUrl: 'assets/partials/index.html',
                controller:  'IndexCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });
            return rp;
        }
    }
]);

