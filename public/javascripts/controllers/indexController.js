var telegramParserAppControllers = angular.module('telegramParserAppControllers',[]);

telegramParserAppControllers.controller('IndexCtrl', ['$scope',
  function($scope) {

    $scope.telegramObject = [];


    $scope.parse = function(){

        $scope.telegramString = $scope.telegramString.substring($scope.telegramString.indexOf("T2AU"));
        $scope.telegramString = $scope.telegramString.substring(0, $scope.telegramString.indexOf("*")+2);

        $scope.telegramObject = [];
        var offset = 0;
        var operationID = "";
        for(var i = 0; i < $scope.telegrams.header.fields.length; i++)
        {
            var field = $scope.telegrams.header.fields[i];
            var value = $scope.telegramString.substring(offset, offset+field.length);
            offset+=field.length;
            $scope.telegramObject.push({name: field.name, value: value, length: field.length});
            if(field.name == "operationID")
            {
                if(value.startsWith("TMA")){
                    operationID = value;
                }else{
                    operationID = value.substring(0,2);
                }
            }
        }

        for(var i = 0; i < $scope.telegrams[operationID].fields.length; i++)
        {
            var field = $scope.telegrams[operationID].fields[i];
            var value = $scope.telegramString.substring(offset, offset+field.length);
            offset+=field.length;
            $scope.telegramObject.push({name: field.name, value: value, length: field.length});
        }

    };

    $scope.parseLog = function(){
        var lines = $scope.logText.split("\n");

        var telegrams = lines.filter(function(val){ return (val.indexOf("RAW Data received") >= 0 || val.indexOf("Sending telegram") >= 0);});
        for(var i = 0; i < telegrams.length; i++)
        {
            telegrams[i] = telegrams[i].substring(telegrams[i].indexOf("T2AU"));
            telegrams[i] = telegrams[i].substring(0, telegrams[i].indexOf("*")+2);
        }

        $scope.logText = telegrams.join("\n");
        console.log(telegrams);

    };

    $scope.onFileSelect = function($files) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
            var reader = new FileReader();

            reader.onload = function(onLoadEvent) {
                $scope.$apply(function() {
                     $scope.logText=onLoadEvent.target.result;
                     $scope.parseLog();
                });
            };

            reader.readAsText($files[i]);
        }
    };

    $scope.telegrams = {
        header: {
            fields:
            [
                {name:'version', length: 2},
                {name:'protocol', length: 2},
                {name:'reserved', length: 2},
                {name:'returnValue', length: 2},
                {name:'datagramLength', length: 6},
                {name:'sourceNode', length: 6},
                {name:'destinationNode', length: 6},
                {name:'datagramSequenceNumber', length: 6},
                {name:'flowControl', length: 2},
                {name:'datagramTimeStamp', length: 22},
                {name:'messageSequenceNumber', length: 6},
                {name:'messageSegmentNumber', length: 6},
                {name:'messageSegmentControl', length: 2},
                {name:'sourceService', length: 6},
                {name:'destinationService', length: 6},
                {name:'operationID', length: 6},
                {name:'operationBlockCount', length: 6},
                {name:'operationBlockLength', length: 6}
            ]
        },
        TMA_NP: {
            fields:[
                {name: 'Order number',    length: 12},
                {name: 'Batch number',    length: 15},
                {name: 'Expiration date', length: 8},
                {name: 'Product name',    length: 36},
                {name: 'Stacking mode',   length: 4},
                {name: 'Number of loaves',length: 6},
                {name: 'Plc Product Id',  length: 9},
                {name: 'Sampling amount', length: 2},
                {name: 'Sampling frequency',length: 2},
                {name: 'Production date',   length: 8},
                {name: 'Short product id',  length: 7},
                {name: 'Gray zone information',length: 1},
                {name: 'Number of batches', length: 2},
                {name: 'Loaves Source', length: 1}
            ]
        },
        TMA_SC: {
            fields:[
                {name:"Order number",  length: 12},
                {name:"Batch number",  length: 15},
                {name:"Sampled loaves",length: 4}
            ]
        },
        TMA_BA: {
            fields:[
                {name: "Order number", length: 12},
                {name: "Batch number", length: 15}
            ]
        },
        TMA_PU: {
            fields:[
                {name:"Plc Material Id",       length: 9},
                {name:"Gray zone information", length: 1},
                {name:"Order number",          length: 12},
                {name:"Production batch",      length: 12}
            ]
        },
        TMA_CL:{
            fields:[
                {name: "Order number", length: 12},
                {name: "RFID", length: 15},
                {name: "Location", length: 14},
                {name: "Crate number", length: 14},
                {name: "End of production", length: 1},
                {name: "Last crate", length: 1},
                {name: "Production timestamp", length: 22},
                {name: "Number of loaves", length: 1},
                {name: "Weight loaf 1", length: 6},
                {name: "Weight loaf 2", length: 6},
                {name: "Weight loaf 3", length: 6},
                {name: "Weight loaf 4", length: 6},
                {name: "Production batch", length: 24}
            ]
        },
        TMA_CP:{
            fields:[
                {name: "Order number", length: 12},
                {name: "RFID", length: 15},
                {name: "Location", length: 14},
                {name: "Crate number", length: 14},
                {name: "End of production", length: 1},
                {name: "Last crate", length: 1},
                {name: "Production timestamp", length: 22},
                {name: "Number of packages", length: 4}
            ]
        },
        TMA_CE:{
            fields:[
                {name: "Order number", length: 12},
                {name: "RFID", length: 15},
                {name: "Location", length: 14},
                {name: "Source layer", length: 2},
                {name: "Destination layer", length: 2}
            ]
        },
        TMA_WT:{
            fields:[
                {name: "Location", length: 14},
                {name: "RFID", length: 16},
                {name: "Weight", length: 12}
            ]
        },
        TMA_PT:{
            fields:[
                {name: "Location", length: 14},
                {name: "RFID", length: 15},
                {name: "Packaging ID", length: 4}
            ]
        },
        TMA_SF:{
            fields:[
                {name: "Location", length: 14}
            ]
        },
        TMA_FP:{
            fields:[
                {name: "Order number", length: 12},
                {name: "RFID", length: 15},
                {name: "Location", length: 14},
                {name: "End of production", length: 1}
            ]
        },
        TMA_BP:{
            fields:[
                {name: "Product name", length: 36},
                {name: "Lot ID", length: 13},
                {name: "Catalog number", length: 16},
                {name: "Total net weight of c", length: 7},
                {name: "Total with cheese", length: 2},
                {name: "RFID", length: 15}
            ]
        },
        TMA_PL:{
            fields:[
                {name: "Layer", length: 2},
                {name: "Crate 1 weight", length: 6},
                {name: "Crate 2 weight", length: 6},
                {name: "Crate 3 weight", length: 6},
                {name: "Crate 4 weight", length: 6}
            ]
        },
        TMA_EP:{
            fields:[
                {name: "Column 1 weight", length: 7},
                {name: "Column 2 weight", length: 7},
                {name: "Column 3 weight", length: 7},
                {name: "Column 4 weight", length: 7}
            ]
        },
        TMA_PP:{
            fields:[
                {name: "Production day", length: 4},
                {name: "Number of crates", length: 2},
                {name: "Number of loaves", length: 3},
                {name: "Product name", length: 36},
                {name: "Catalog number", length: 16},
                {name: "Passover kosher", length: 1},
                {name: "Kosher level", length: 1},
                {name: "Expiration date", length: 8},
                {name: "Barcpde", length: 13},
                {name: "Net weight", length: 6},
                {name: "Day in week", length: 1},
                {name: "Production batch", length: 24},
                {name: "RFID", length: 15},
                {name: "Quarantine", length: 1},
                {name: "Loaves/Packs per crate", length: 2}
            ]
        },
        TT: {
            fields:[
                {name: 'SUNumber', length: 20},
                {name: 'Source Address', length: 14},
                {name: 'Source grid', length: 2},
                {name: 'Source position', length: 2},
                {name: 'Destination address', length: 14},
                {name: 'Destination grid', length: 2},
                {name: 'Destination position', length: 2},
                {name: 'SUWeight Class', length: 2},
                {name: 'SUHeight Class', length: 2},
                {name: 'SUType', length: 4},
                {name: 'LPKZ', length: 2},
                {name: 'SUWeight', length: 3},
                {name: 'SUOverSizeXFront', length: 3},
                {name: 'SUOverSizeXBack', length: 3},
                {name: 'SUOverSizeZLeft', length: 3},
                {name: 'SUOverSizeZRight', length: 3},
                {name: 'SUHeight', length: 4},
                {name: 'Material ID', length: 18},
                {name: 'Station', length: 14},
                {name: 'Parameter', length: 6},
                {name: 'ErrorCode', length: 2},
                {name: 'ErrorReason', length: 4},
                {name: 'Movement ID', length: 9},
            ]
        },
        AT: {
            fields:[
                {name: 'mode', length: 2},
                {name: 'Time', length: 22},
                {name: 'Parameter', length: 6}
            ]
        },
        SA: {
            fields:[
                {name: 'Betriebsart', length: 2},
                {name: 'Störunggruppe', length: 2},
                {name: 'Störungskode', length: 7},
                {name: 'Auftragszustand', length: 2},
                {name: 'Error Additional', length: 6},
                {name: 'Table 1 phase', length: 2},
                {name: 'Table 2 phase', length: 2},
                {name: 'LogicalCoord1', length: 6},
                {name: 'PhysicalCoord1', length: 6},
                {name: 'LogicalCoord2', length: 6},
                {name: 'PhysicalCoord2', length: 6},
                {name: 'LogicalCoord3', length: 6},
                {name: 'PhysicalCoord3', length: 6},
                {name: 'CfgMode', length: 6}
            ]
        },
        SB: {
            fields:[
                {name: 'Betriebsart', length: 2},
                {name: 'Bit Count ', length: 3},
                {name: 'Alarm Bits', length: 133}
            ]
        },
        TS: {
            fields:[
                {name: 'SUNumber', length: 20},
                {name: 'StageSource', length: 14},
                {name: 'SUWeightClass', length: 2},
                {name: 'SUType', length: 4},
                {name: 'MovementID', length: 9},
                {name: 'MovementStatus', length: 2},
                {name: 'DataValid', length: 2},
                {name: 'materialId', length: 18},
                {name: 'filler', length: 69},
            ]
        },
        TL: {
            fields:[
                {name: 'Message', length: 138}
            ]
        },
        TM: {
            fields:[
                {name: 'Payload', length: 138}
            ]
        },
        TH: {
            fields:[
                {name: 'TU_NUMBER', length: 20},
                {name: 'TU_TYPE', length: 2},
                {name: 'TU_SUBTYPE', length: 2},
                {name: 'TU_GRID_X', length: 4},
                {name: 'TU_GRID_Y', length: 4},
                {name: 'TU_GRID_Z', length: 4},
                {name: 'TU_POS_X', length: 4},
                {name: 'TU_POS_Y', length: 4},
                {name: 'TU_POS_Z', length: 4},
                {name: 'LOCATION', length: 14},
                {name: 'LOCAT_GRID', length: 2},
                {name: 'LOCAT_POS', length: 2},
                {name: 'MAT_ID', length: 18},
                {name: 'MAT_TYPE', length: 2},
                {name: 'MAT_LENGTH', length: 4},
                {name: 'MAT_WIDTH', length: 4},
                {name: 'MAT_HEIGHT', length: 4},
                {name: 'MAT_WEIGHT', length: 4},
                {name: 'QTY_REQ', length: 3},
                {name: 'QTY_DONE', length: 3},
                {name: 'ERR_CODE', length: 2},
                {name: 'ERR_REASON', length: 4},
                {name: 'PARAMETER 1', length: 6},
                {name: 'PARAMETER 2', length: 6},
                {name: 'OPERATION_ID', length: 9}
            ]
        },
        TC: {
            fields:[
                {name: 'LOCATION', length: 10},
                {name: 'DESTINATION', length: 10},
                {name: 'RESTPIECE DESTINATION', length: 10},
                {name: 'MAT_ID', length: 6},
                {name: 'MAT_TYPE', length: 2},
                {name: 'MAT_LENGTH', length: 4},
                {name: 'MAT_WIDTH', length: 4},
                {name: 'MAT_HEIGHT', length: 4},
                {name: 'MAT_WEIGHT', length: 4},
                {name: 'MAT_STRENGTH', length: 2},
                {name: 'MAT_SHAPE', length: 2},
                {name: 'CUT_PROFILE_ID', length: 3},
                {name: 'POWER_FACTOR', length: 3},
                {name: 'CUT_LENGTH', length: 6},
                {name: 'QTY_REQ', length: 3},
                {name: 'QTY_DONE', length: 3},
                {name: 'ANGLE1', length: 3},
                {name: 'ANGLE1_LENGTH', length: 4},
                {name: 'ANGLE2', length: 3},
                {name: 'ANGLE2_LENGTH', length: 4},
                {name: 'ANGLE3', length: 3},
                {name: 'ANGLE3_LENGTH', length: 4},
                {name: 'ANGLE4', length: 3},
                {name: 'ANGLE4_LENGTH', length: 4},
                {name: 'CUT_TYPE', length: 2},
                {name: 'ERR_CODE', length: 2},
                {name: 'ERR_REASON', length: 4},
                {name: 'PARAMETER 1', length: 4},
                {name: 'OPERATION_ID', length: 9}
            ]
        }
    }
}]);